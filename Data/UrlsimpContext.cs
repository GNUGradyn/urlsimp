﻿using Microsoft.EntityFrameworkCore;

namespace urlsimp.Data
{
    public class UrlsimpContext : DbContext
    {
        private IConfiguration _config { get; set; }
        public DbSet<Models.Link> Links { get; set; }
        public UrlsimpContext(IConfiguration configuration)
        {
            _config = configuration;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            var provider = _config.GetValue<DatabaseProvider>("Database:Provider");
            var connectionString = _config.GetValue<string>("Database:ConnectionString");
            if (provider == DatabaseProvider.sqlite) options.UseSqlite(connectionString);
            if (provider == DatabaseProvider.postgre) options.UseNpgsql(connectionString);
        }
    }
}
