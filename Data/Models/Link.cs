﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace urlsimp.Data.Models
{
    [Table("Link")]
    public class Link
    {
        public string Url { get; set; }
        public bool IsVanity { get; set; }
        [Key]
        public string Key { get; set; }
        public string Ip { get; set; }
        public string? Password { get; set; }
    }
}
