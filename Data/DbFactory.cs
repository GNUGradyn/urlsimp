﻿namespace urlsimp.Data
{
    public class DbFactory
    {
        private IConfiguration _config;
        public DbFactory(IConfiguration configuration)
        {
            _config = configuration;
            // Make sure the tables and database and stuff exist
            var ctx = new UrlsimpContext(_config);
            ctx.Database.EnsureCreated();
            ctx.SaveChanges();
        }
        public UrlsimpContext UrlsimpData()
        {
            return new UrlsimpContext(_config);
        }
    }
}
