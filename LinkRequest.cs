﻿namespace urlsimp
{
    public class LinkRequest
    {
        public string Url { get; set; }
        public string? Key { get; set; }
        public string? Password { get; set; }
    }
}
