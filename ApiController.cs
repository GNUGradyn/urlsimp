﻿using Microsoft.AspNetCore.Mvc;
using urlsimp.Data;

namespace urlsimp
{
    [ApiController]
    public class ApiController : ControllerBase
    {
        private static Random random = new Random();
        private IConfiguration _config { get; set; }
        private DbFactory _db;
        private static string KeyGenerator(int length) // TODO: Prevent duplicate keys from being generated
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public ApiController(IConfiguration configuration, DbFactory dbFactory)
        {
            _config = configuration;
            _db = dbFactory;
        }

        [Route("/save")]
        [HttpPost]
        public JsonResult ShortenUrl(LinkRequest request)
        {
            var key = string.IsNullOrEmpty(request.Key) ? KeyGenerator(_config.GetValue<int>("DefaultKeyLength")) : request.Key;
            using (var db = _db.UrlsimpData())
            {
                bool IsVanity = false;
                if (request.Key == "save") return new JsonResult("Key is invalid");
                if (request.Url == null) return new JsonResult("No Url specified") { StatusCode = 400 };
                if (!string.IsNullOrEmpty(request.Key)) IsVanity = true;
                if (db.Links.Find(request.Key) != null) return new JsonResult("Key already in use") { StatusCode = 400 }; // TODO: Add an endpoint to check this so the user knows if its available in real time
                var existingLinks = db.Links.Where(x => x.Url == request.Url && x.IsVanity == false && string.IsNullOrEmpty(x.Password)); // TODO: if password AND link AND key are the same, it is a duplicate request so resend existing key
                if (existingLinks.Any() && !IsVanity && String.IsNullOrEmpty(request.Password))
                {
                    return new JsonResult(existingLinks.First().Key);
                }
                var link = db.Links.Add(new Data.Models.Link
                {
                    Url = request.Url,
                    Key = key,
                    Ip = Request.Headers["X-Forwarded-For"].ToString() ?? Request.HttpContext.Connection.RemoteIpAddress.ToString(),
                    IsVanity = IsVanity,
                    Password = request.Password
                });
                db.SaveChanges();
            }
            return new JsonResult(key);
        }
    }
}
