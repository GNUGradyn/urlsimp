using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.FileProviders;
using urlsimp;
using urlsimp.Data;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllers();
builder.Services.AddSingleton<DbFactory>();
var app = builder.Build();    

app.UseFileServer();

// Inject config into databse context TODO: use dependency injection for this

app.MapControllers();
app.Run($"http://0.0.0.0:{builder.Configuration.GetValue<int>("Port")}");
